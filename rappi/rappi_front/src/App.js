import React, { Component } from 'react';
import {Map, InfoWindow, Marker, GoogleApiWrapper, HeatMap} from 'google-maps-react';

import './App.css';
//import Orden from './ordenes.js'
class App extends Component {

constructor(props){
  super(props);

  this.state = {
    ordenes : [],
    positions:[]
  };
}

componentDidMount(){
  fetch("/getData")
  .then((response) => {
    return response.json()
  })
  .then((json) => this.setState({ordenes:json}))
  .catch((err) => console.log(err));
}
  renderRappi(){
    return this.state.ordenes.map((ord)=> 
      //<Orden orden={ord}/>
      //
      <div key={ord.id}> {ord.lat} - {ord.lng} - {ord.timestamp}- {ord.type} </div>
      );
  }

  renderMap(){
    //<Marker

     //           name={'Current location'} 
   //             position={{lat:ord.lat, lng: ord.lng}} 
///
            ///>
   const gradient = [
    'rgba(0, 255, 255, 0)',
    'rgba(0, 255, 255, 1)',
    'rgba(0, 191, 255, 1)',
    'rgba(0, 127, 255, 1)',
    'rgba(0, 63, 255, 1)',
    'rgba(0, 0, 255, 1)',
    'rgba(0, 0, 223, 1)',
    'rgba(0, 0, 191, 1)',
    'rgba(0, 0, 159, 1)',
    'rgba(0, 0, 127, 1)',
    'rgba(63, 0, 91, 1)',
    'rgba(127, 0, 63, 1)',
    'rgba(191, 0, 31, 1)',
    'rgba(255, 0, 0, 1)'
  ];
    console.log("oooo")
    return ( this.state.ordenes.map((ord)=> 
      <Map key={ord.id} google={this.props.google} zoom={14}>
        <Marker >

                name={'Current location'} 
                position={{lat:ord.lat, lng: ord.lng}} 

            </Marker>
        
      </Map>));
  }
  render() {
    return (
      <div className="App">
       <h1>Rappi Hack</h1>
     
       {this.renderMap()}
      </div>
    );
  }
}


export default GoogleApiWrapper({
  apiKey: ('AIzaSyA00lwQQgWZXTKZ_UV4V9o3yT1XRdnEAlY')
})(App)
