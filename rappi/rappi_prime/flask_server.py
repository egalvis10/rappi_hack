from flask import Flask
import os.path
import requests
import json
from pprint import pprint
import pandas as pd
import numpy as np

app = Flask(__name__)

RAPPITENDEROS = 'rappitenderos.npy'
ORDENES = 'ordenes.npy'
DATA = 'data'
ENDPOINT_TENDEROS = 'http://40.117.153.177:3001/getDataTwo'
ENDPOINT_ORDENES = 'http://40.117.153.177:3001/getData'
JSON= 'data/phraseFreqs.json'
rta_1 =os.path.join(DATA, ORDENES)
rta_2 = os.path.join(DATA, RAPPITENDEROS)

ret_array= []

@app.route("/getRappiPrime")
def hello():
	if os.path.isfile(rta_1):
		print('ENTROOO A primero')
		with open(rta_1) as ordenes:
			ret_array.append(ordenes)
	if os.path.isfile(rta_1)==False:
		print('ENTRO A SEGUNDO')
		with requests.Session() as r:
			r = r.get(ENDPOINT_ORDENES).json()
			print(type(r))
			print(r[0])
			np.save('data/ordenes.npy',r)
	if os.path.isfile(rta_2):
		with open(rta_2) as rappitenderos:
			ret_array.append(rappitenderos)
	if os.path.isfile(rta_2)==False:
		print('entro a tercero')
		with requests.Session() as r:
			r = r.get(ENDPOINT_TENDEROS).json()
			print(len(r))
			with open('data/rappitenderos.npy', 'wb') as np_file:
				np.save(np_file, r)

@app.route("/getRappitenderos")
def backup():
		ret = np.load(rta_2)
		print("Rappitendero")
		print(len(ret))
		print(type(ret))
		print("Ordenes")
		ret_2 = np.load(rta_1)
		print(len(ret_2))
		print(type(ret_2))


if __name__ == '__main__':
	app.run(host='0.0.0.0',debug=False)
