var express = require('express');
var router = express.Router();

const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

const { Pool, Client } = require('pg')


const findDocuments = function(db, callback) {
  // Get the documents collection
  const collection = db.collection('orders');
  // Find some documents
  collection.find({} , 
    {fields:{_id:0}}
    ).toArray(function(err, docs) {
    assert.equal(err, null);
    console.log("Found the following records");
    console.log(docs)
    callback(docs);
  });
}

function getConsulta(callback){

// Connection URL
const url = 'mongodb://hackathonmongo:hackathon2018rappimongodb@mongo-hackathon.eastus2.cloudapp.azure.com:27017/orders';
console.log(url)
// Database Name
const dbName = 'orders';
// Use connect method to connect to the server
MongoClient.connect(url, function(err, client) {
  assert.equal(null, err);
  console.log("Connected successfully to server");

  const db = client.db(dbName);
  findDocuments(db, (data)=>{
    callback(data);
    client.close(); 
  });
  
});
}

function getConsultaDos(callback){

const client = new Client({
  user: 'hackathonpostgres',
  host: 'postgres-hackathon.eastus2.cloudapp.azure.com',
  database: 'storekeepersdb',
  password: 'hackathon2018rappipsql',
  port: 5432,
});
client.connect();

client.query('SELECT storekeeper_id, lat, lng, timestamp, toolkit FROM storekeepers', (err, res) => {
  console.log(err, res)
  client.end()
});

}


/* GET datos MongoDB ordenes. */
router.get('/getDataTwo', function(req, res) {
 getConsultaDos((data)=>
  res.send(data)
  );
});

/* GET datos MongoDB ordenes. */
router.get('/getData', function(req, res) {
 getConsulta((data)=>
  res.send(data)
  );
});

module.exports = router;